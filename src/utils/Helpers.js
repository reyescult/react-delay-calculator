export const UniqueId = () => Math.random().toString(36).substr(2, 16); // http://www.frontcoded.com/javascript-create-unique-ids.html

export const getPresets = () => {
  const presetsData = localStorage.getItem('presets');
  const presets = presetsData == null ? [] : presetsData.includes('-') ?
                  JSON.parse(presetsData).filter(item => item !== '-').map(item => ({ id: UniqueId(), value: item })) :
                  JSON.parse(presetsData);
  return presets;
};

export const setPresets = () => {
  const presetsData = localStorage.getItem('presets');
  if (presetsData == null || presetsData.includes('-')) {
    localStorage.setItem('presets', JSON.stringify(getPresets()));
  }
}

export const putPresets = (presets) => {
  localStorage.setItem('presets', JSON.stringify(presets));
};

export const getTimes = (bpm) => {
  const crotchet = (bpm > 0) ? (60000 / bpm) : 0;
  const notes = [
    {
      note: '1/1',
      duration: (crotchet * 4)
    },
    {
      note: '1/2',
      duration: (crotchet * 2)
    },
    {
      note: '1/4',
      duration: crotchet
    },
    {
      note: '1/8',
      duration: (crotchet / 2)
    },
    {
      note: '1/16',
      duration: (crotchet / 4)
    },
    {
      note: '1/32',
      duration: (crotchet / 8)
    },
    {
      note: '1/64',
      duration: (crotchet / 16)
    },
    {
      note: '1/128',
      duration: (crotchet / 32)
    },
    {
      note: '1/256',
      duration: (crotchet / 64)
    }
  ];
  return {
    straight: notes.map((note) => ({
      note: note.note,
      duration: note.duration.toFixed(2)
    })),
    dotted: notes.map((note) => ({
      note: note.note + ' D',
      duration: (note.duration * 1.5).toFixed(2)
    })),
    triplet: notes.map((note) => ({
      note: note.note + ' T',
      duration: (note.duration * 0.667).toFixed(2)
    }))
  }
}
