const localStorageMock = {
  getItem: jest.fn(),
  setItem: jest.fn(),
  clear: jest.fn()
};
const matchMediaMock = () => ({
  matches: false,
  addListener: jest.fn(),
  removeListener: jest.fn()
});
global.localStorage = localStorageMock
global.matchMedia = matchMediaMock