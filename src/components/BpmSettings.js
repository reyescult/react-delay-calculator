import React from 'react';
import Button from './Button';
import BpmPresets from './BpmPresets';

function Heading() {
  return (
    <React.Fragment>
      <h4>Manage Presets</h4>
      <p>Select a preset to remove it from memory.</p>
    </React.Fragment>
  );
}

class Dialog extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      showPresetsTop: false,
      showPresetsBtm: true
    }
    this.handleScroll = this.handleScroll.bind(this);
  }
  handleScroll(e) {
    const el = e.currentTarget;
    this.setState({showPresetsTop: el.scrollTop > 0});
    this.setState({showPresetsBtm: el.scrollTop !== el.offsetHeight - 1});
  }
  render() {
    return (
      <div className={'bpm-dialog' + (this.state.showPresetsTop ? ' show-presets-top' : '') + (this.state.showPresetsBtm ? ' show-presets-btm' : '')}>
        <Button class="bpm-close-button" ariaLabel="Close settings" handleClick={this.props.toggleSettings} />
        <Heading />
        <BpmPresets state={this.props.state} containerClass="bpm-delete-buttons" buttonClass="bpm-delete-button" handleClick={this.props.handleDelete} handleScroll={this.handleScroll} />
        <Button class="bpm-delete-all-button" label="Delete All Presets" disabled={this.props.state.presets.length === 0} handleClick={this.props.handleDeleteAll} />
      </div>
    );
  }
}

function BpmSettings(props) {
  if (props.state.showSettings) {
    return (
      <aside className="bpm-settings">
        <Dialog state={props.state} handleDelete={props.handleDelete} handleDeleteAll={props.handleDeleteAll} toggleSettings={props.toggleSettings} />
      </aside>
    );
  }
  return null;
}

export default BpmSettings;
