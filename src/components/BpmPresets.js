import React from 'react';
import Button from './Button';

function BpmPresets(props) {
  const used = props.state.presets.map(
    preset => <Button class={props.state.bpm === preset.value ? props.buttonClass + ' current' : props.buttonClass} key={preset.id} label={preset.value} handleClick={props.handleClick} />
  );
  const unused = Array(props.state.presetsMax - props.state.presets.length).fill(null).map(
    (item, index) => <Button class={props.buttonClass} key={index} label="-" disabled />
  );
  const presets = used.concat(unused);
  return (
    <div className={props.containerClass} style={props.containerStyle} onScroll={props.handleScroll}>
      {presets}
    </div>
  );
}

export default BpmPresets;
