import React from 'react';

function BpmLogo() {
  return <p className="bpm-logo"><span>Delay Calculator</span></p>;
}

export default BpmLogo;