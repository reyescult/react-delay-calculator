import React from 'react';

function Button(props) {
  return <button type="button" className={props.class} value={props.label} aria-label={props.ariaLabel} disabled={props.disabled} onClick={props.handleClick}>{props.label}</button>;
}

export default Button;
