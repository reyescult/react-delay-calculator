import React from 'react';
import BpmLogo from './BpmLogo';

function BpmFooter(props) {
  const date = new Date().getFullYear();
  return (
    <footer className="bpm-footer">
      <p className="footer-info copy">&copy; 2016-{date} Reyescult DMD. All Rights Reserved.</p>
      <BpmLogo />
      <p className="footer-info disc">* This app uses local storage<span> for preset memory</span></p>
    </footer>
  );
}

export default BpmFooter;
