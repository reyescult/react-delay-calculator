import React from 'react';
import Button from './Button';
import BpmPresets from './BpmPresets';

function BpmPresetsMenu(props) {
  const bpmPresetsStyle = {
    marginLeft: props.state.presetsPagePosition + '%'
  };
  const disablePrevPage = props.state.presetsPagePosition === 0;
  const disableNextPage = props.state.presetsPagePosition === -(props.state.presetsMax / props.state.presetsPerPage * 100 - 100);
  return (
    <div className="bpm-presets-menu" hidden={!props.state.showPresets}>
      <Button class="bpm-page-button prev" ariaLabel="previous page" disabled={disablePrevPage} handleClick={props.handlePrevPage} />
      <div className="bpm-presets-list">
          <BpmPresets state={props.state} containerClass="bpm-load-buttons" containerStyle={bpmPresetsStyle} buttonClass="bpm-load-button" handleClick={props.handleLoad} />
      </div>
      <Button class="bpm-page-button next" ariaLabel="next page" disabled={disableNextPage} handleClick={props.handleNextPage} />
    </div>
  )
}

export default BpmPresetsMenu;
