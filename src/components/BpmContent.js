import React from 'react';

function BpmStatus(props) {
  if (!props.state.bpm) {
    return (
      <div className="bpm-status">
        <h2>It's tempo time!</h2>
        <p>Please enter a valid tempo<br />in the field above.</p>
      </div>
    );
  }
  return null;
}

function BpmNotes(props) {
  if (props.state.bpm) {
    return (
      <div className='bpm-notes'>
        <BpmList heading="Straight" state={props.state} times={props.state.times.straight} />
        <BpmList heading="Dotted" state={props.state} times={props.state.times.dotted} />
        <BpmList heading="Triplet" state={props.state} times={props.state.times.triplet} />
      </div>
    );
  }
  return null;
}

function BpmList(props) {
  const presetClass = props.state.presets.find(preset => preset.value === props.state.bpm) ? ' preset' : '';
  return (
    <div className={'bpm-list' + presetClass}>
      <h3><span className="label">{props.heading}</span></h3>
      <ul>{props.times.map((time) => <li key={time.note}><span className="label">{time.note}</span><span className="value">{time.duration + ' ms'}</span></li>)}</ul>
    </div>
  );
}

function BpmContent(props) {
  return (
    <main className="bpm-content">
      <BpmNotes state={props.state} />
      <BpmStatus state={props.state} />
    </main>
  );
}

export default BpmContent;
