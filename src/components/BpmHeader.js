import React from 'react';
import Button from './Button';
import BpmLogo from './BpmLogo';

function BpmForm(props) {
  const presetClass = props.state.presets.find(preset => preset.value === props.state.bpm) ? ' preset' : '';
  return (
    <div className={'bpm-form' + presetClass}>
      <label htmlFor="bpm-input">
        Tempo
        <input type="number" id="bpm-input" value={props.state.bpm ? props.state.bpm : ''} onChange={props.handleChange} autoFocus />
      </label>
    </div>
  );
}

function BpmMenu(props) {
  const saveDisabled = props.state.presets.length === props.state.presetsMax || props.state.presets.find(preset => preset.value === props.state.bpm) || props.state.bpm === null ? true : false;
  return (
    <div className="bpm-menu">
      <Button class="bpm-save-button" ariaLabel="save preset" label="Save" disabled={saveDisabled} handleClick={props.handleSave} />
      <Button class="bpm-presets-button" ariaLabel="toggle presets menu" label="Presets" handleClick={props.togglePresets} />
      <Button class="bpm-settings-button" ariaLabel="open user settings" label="Settings" handleClick={props.toggleSettings} />
    </div>
  );
}

function BpmHeader(props) {
  return (
    <header className="bpm-header">
      <BpmForm state={props.state} handleChange={props.handleChange} />
      <BpmMenu state={props.state} handleSave={props.handleSave} togglePresets={props.togglePresets} toggleSettings={props.toggleSettings} />
      <BpmLogo />
    </header>
  );
}

export default BpmHeader;
