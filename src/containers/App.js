import React, { Component } from 'react';
import './App.css';
import BpmHeader from '../components/BpmHeader';
import BpmPresetsMenu from '../components/BpmPresetsMenu';
import BpmContent from '../components/BpmContent';
import BpmSettings from '../components/BpmSettings';
import BpmFooter from '../components/BpmFooter';
import * as Helpers from '../utils/Helpers';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bpm: null,
      media: null,
      presets: Helpers.getPresets(),
      presetsMax: 32,
      presetsPerPage: null,
      presetsPagePosition: null,
      times: null,
      showPresets: true,
      showSettings: false
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleDeleteAll = this.handleDeleteAll.bind(this);
    this.handleLoad = this.handleLoad.bind(this);
    this.handleNextPage = this.handleNextPage.bind(this);
    this.handlePrevPage = this.handlePrevPage.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.togglePresets = this.togglePresets.bind(this);
    this.toggleSettings = this.toggleSettings.bind(this);
    Helpers.setPresets();
  }

  componentDidMount() {
    const mql = [
      window.matchMedia('(max-width: 767px)'),
      window.matchMedia('(max-width: 1079px)')
    ];
    const matchMedia = () => {
      const match = mql[0].matches ? 'mobile' : (mql[1].matches ? 'tablet' : 'desktop');
      this.setState({
        media: match,
        presetsPerPage: match === 'mobile' ? 4 : (match === 'tablet' ? 8 : 16),
        presetsPagePosition: 0
      });
    };
    matchMedia();
    mql.forEach(item => item.addListener(matchMedia));
  }

  handleChange(e) {
    const etv = e.target.value;
    this.setState({
      bpm: etv ? etv : null,
      times: etv ? Helpers.getTimes(etv) : null
    });
  }

  handleDelete(e) {
    const confirm = window.confirm('Are you sure you want to delete this preset?');
    const presets = this.state.presets.slice();
    if (confirm === true) {
      presets.splice(presets.findIndex(preset => preset.value === e.target.value), 1);
      this.setState({ presets: presets });
      Helpers.putPresets(presets);
    }
  }

  handleDeleteAll() {
    const confirm = window.confirm('Are you sure you want to delete all of your presets?');
    const presets = [];
    if (confirm === true) {
      this.setState({ presets: presets });
      Helpers.putPresets(presets);
    }
  }

  handleLoad(e) {
    const etv = e.target.value;
    this.setState({
      bpm: etv,
      times: Helpers.getTimes(etv)
    });
  }
  
  handleNextPage() {
    this.setState({ presetsPagePosition: this.state.presetsPagePosition - 100 });
  }
  
  handlePrevPage() {
    this.setState({ presetsPagePosition: this.state.presetsPagePosition + 100 });
  }

  handleSave() {
    const presets = this.state.presets.slice();
    if (presets.length < this.state.presetsMax) {
      presets.push({ id: Helpers.UniqueId(), value: this.state.bpm });
    }
    this.setState({ presets: presets });
    Helpers.putPresets(presets);
  }

  togglePresets() {
    this.setState({ showPresets: !this.state.showPresets });
  }

  toggleSettings() {
    this.setState({ showSettings: !this.state.showSettings });
  }
  
  render() {
    document.body.className = this.state.media + '-media';
    return (
      <React.Fragment>
        <BpmHeader state={this.state} handleChange={this.handleChange} handleKeyPress={this.handleKeyPress} handleSave={this.handleSave} togglePresets={this.togglePresets} toggleSettings={this.toggleSettings} />
        <BpmPresetsMenu state={this.state} handleLoad={this.handleLoad} handleNextPage={this.handleNextPage} handlePrevPage={this.handlePrevPage} />
        <BpmContent state={this.state} />
        <BpmSettings state={this.state} handleDelete={this.handleDelete} handleDeleteAll={this.handleDeleteAll} toggleSettings={this.toggleSettings} />
        <BpmFooter />
      </React.Fragment>
    );
  }
}

export default App;
