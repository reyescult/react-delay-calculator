// Methods

class Methods {

  handleSave() {
    const presets = this.state.presets.slice();
    presets.splice(presets.findIndex(preset => preset.value === null), 1, {
      id: presets.find(preset => preset.value === null).id,
      value: this.state.bpm
    });
    this.setState({ presets: presets });
    Helpers.putPresets(presets);
  }

}

// Constants

const saveDisabled = props.state.presets.find(preset => preset.value === null) && props.state.bpm !== null ? false : true;

const saveDisabled = !props.state.presets.find(preset => preset.value === null) || props.state.presets.find(preset => preset.value === props.state.bpm) || props.state.bpm === null ? true : false;

export const initStorage = () => localStorage.setItem('presets', JSON.stringify(initPresets));
